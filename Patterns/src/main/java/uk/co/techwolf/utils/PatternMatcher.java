package uk.co.techwolf.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {
	public String[] findPattern(String line, String regex) {
		String[] groups = new String[] {};
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		boolean matchFound = matcher.find();
		if (matchFound) {
			int groupCount = matcher.groupCount();
			groups = new String[groupCount];
			for (int group = 0; group < groups.length; group++) {
				groups[group] = matcher.group(group + 1);
			}
		}
		return groups;
	}
}