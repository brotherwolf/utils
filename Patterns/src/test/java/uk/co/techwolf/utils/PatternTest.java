package uk.co.techwolf.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

public class PatternTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSimpleMatch() {
		String line = "a b c d";
		String regex = "(b)";
		String expected = "b";
		assertMatch(line, regex, expected);
	}


	@Test
	public void testNumberMatch() {
		String line = "Hi there number 6, how are you?";
		String regex = ".*number\\ (\\d+).*";
		String expected = "6";
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testActualComplexMatch() {
		String line = "2012-07-11 16:49:55,139+0100 [] WARN [DefaultScheduler_Worker-6]  [org.alfresco.repo.node.index.AVMRemoteSnapshotTracker] processStores ran in:136:";
		String regex = ".*DefaultScheduler\\_Worker\\-(\\d+).*";
		String expected = "6";
		assertMatch(line, regex, expected);
	}

	public void assertMatch(String line, String regex, String expected) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(line);
		boolean matchFound = matcher.find();
		assertTrue("Should find something", matchFound);
		
		String id = matcher.group(1);
		assertEquals("first match should be " + expected, expected, id);
	}
	
	@Test
	public void testMatchHtmlTag() {
		// Compile regular expression with a back reference to group 1 
		String patternStr = "<(\\S+?).*?>[\\ ]*(.*?)[\\ ]*</\\1>"; 
		Pattern pattern = Pattern.compile(patternStr); 
		Matcher matcher = pattern.matcher(""); 

		// Set the input 
		matcher.reset("xx <tag a=b> yy </tag> zz"); 

		// Get tagname and contents of tag 
		boolean matchFound = matcher.find();  
		assertTrue("Should find match", matchFound); 

		String tagname = matcher.group(1); 
		assertEquals("Should find tag name", "tag", tagname);

		String contents = matcher.group(2); 
		assertEquals("Should find contents of tag", "yy", contents);

		matcher.reset("xx <tag> yy </tag0>"); 

		matchFound = matcher.find(); // false
		assertFalse("Should NOT find match", matchFound); 
	}
	
	@Test
	public void testMatchHtmlTagWithMyFunction() {
		// Compile regular expression with a back reference to group 1 
		String regex = "<(\\S+?).*?>[\\ ]*(.*?)[\\ ]*</\\1>"; 
		String line = "xx <tag a=b> yy </tag> zz"; 
		String expected = "tag";
		assertMatch(line, regex, expected);
	}
	
	
	@Test
    public void testBasicMatch() {
        // Compile regular expression
        String patternStr = "b";
        Pattern pattern = Pattern.compile(patternStr);

        // Determine if pattern exists in input
        CharSequence inputStr = "a b c b";
        Matcher matcher = pattern.matcher(inputStr);
        
        boolean matchFound = matcher.find();
        assertTrue("Match should be found", matchFound);
        
        // Get matching string
        String match = matcher.group();
        assertEquals("Match should be 'b'", "b", match);
        
        // Get indices of matching string
        int start = matcher.start();
        assertEquals(2, start);
        
        int end = matcher.end();
        assertEquals(3, end);
        // the end is index of the last matching character + 1

        // Find the next occurrence
        matchFound = matcher.find();
        assertTrue("Match should be found", matchFound);
    }
}
