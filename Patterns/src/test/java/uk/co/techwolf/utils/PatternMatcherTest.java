package uk.co.techwolf.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class PatternMatcherTest {

	private static final String CAPITAL_LETTER_STRING_WITH_MIN_7_LETTERS_PATTERN = "^([A-Z]{7,})$";
	private PatternMatcher matcher;

	@Before
	public void setUp() throws Exception {
		matcher = new PatternMatcher();
	}

	@Test
	public void testSimpleMatch() {
		String line = "a b c d";
		String regex = "(b)";
		String[] expected = new String[] { "b" };
		assertMatch(line, regex, expected);
	}


	@Test
	public void testNumberMatch() {
		String line = "Hi there number 6, how are you?";
		String regex = ".*number\\ (\\d+).*";
		String[] expected = new String[] {"6"};
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testActualComplexMatch() {
		String line = "2012-07-11 16:49:55,139+0100 [] WARN [DefaultScheduler_Worker-6]  [org.alfresco.repo.node.index.AVMRemoteSnapshotTracker] processStores ran in:136:";
		String regex = ".*DefaultScheduler\\_Worker\\-(\\d+).*";
		String[] expected = new String[] {"6"};
		assertMatch(line, regex, expected);
	}

	public void assertMatch(String line, String regex, String[] expected) {
		String[] results = matcher.findPattern(line, regex);
		if (results == null && expected != null) {
			fail("No matches found");
		} else if (results.length != expected.length) {
			fail("number of results does not match expected results (expected: " + expected.length + ":  actual:" + results.length + ":)");
		} else {
			for (int result = 0; result < results.length; result++) {
				assertEquals(expected[result], results[result]);
			}
		}
	}
	
	@Test
	public void testMatchHtmlTagWithMyFunction() {
		// Compile regular expression with a back reference to group 1 
		String regex = "<(\\S+?).*?>[\\ ]*(.*?)[\\ ]*</\\1>"; 
		String line = "xx <tag a=b> yy </tag> zz"; 
		String[] expected = new String[] {"tag", "yy"};
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testCanMatchStringContainingOnlyCapitalLetters() {
		String line = "WELCOME";
		String regex = CAPITAL_LETTER_STRING_WITH_MIN_7_LETTERS_PATTERN;
		String[] expected = new String[] {"WELCOME"};
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testDoesNotMatchStringContainingTooFewCapitalLetters() {
		String line = "WELCOM";
		String regex = CAPITAL_LETTER_STRING_WITH_MIN_7_LETTERS_PATTERN;
		String[] expected = new String[] {};
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testDoesNotMatchEmptyString() {
		String line = "";
		String regex = CAPITAL_LETTER_STRING_WITH_MIN_7_LETTERS_PATTERN;
		String[] expected = new String[] {};
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testDoesNotMatchStringContainingMixedCaseLetters() {
		String line = "WelcomeBack";
		String regex = CAPITAL_LETTER_STRING_WITH_MIN_7_LETTERS_PATTERN;
		String[] expected = new String[] {};
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testDoesNotMatchStringContainingDigits() {
		String line = "W3LC0M38ACK";
		String regex = CAPITAL_LETTER_STRING_WITH_MIN_7_LETTERS_PATTERN;
		String[] expected = new String[] {};
		assertMatch(line, regex, expected);
	}
	
	@Test
	public void testDoesNotMatchStringContainingSymbols() {
		String line = "WE|COMEBACK";
		String regex = CAPITAL_LETTER_STRING_WITH_MIN_7_LETTERS_PATTERN;
		String[] expected = new String[] {};
		assertMatch(line, regex, expected);
	}
	
}
